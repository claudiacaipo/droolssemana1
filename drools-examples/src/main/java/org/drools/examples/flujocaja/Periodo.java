package org.drools.examples.flujocaja;

import java.util.Date;

public class Periodo {

	private Date inicio;
	private Date fin;
	
	public Periodo(Date inicio, Date fin){
		this.inicio = inicio;
		this.fin = fin;
	}
	public Date getFin() {
		return fin;
	}
	public void setFin(Date fin) {
		this.fin = fin;
	}
	public Date getInicio() {
		return inicio;
	}
	public void setInicio(Date inicio) {
		this.inicio = inicio;
	}
}
