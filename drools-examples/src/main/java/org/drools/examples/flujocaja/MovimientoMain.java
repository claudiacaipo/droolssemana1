package org.drools.examples.flujocaja;
import org.kie.api.KieServices;
import org.kie.api.event.rule.DebugAgendaEventListener;
import org.kie.api.event.rule.DebugRuleRuntimeEventListener;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.rule.FactHandle;

import java.text.SimpleDateFormat;
import java.util.Date;

public class MovimientoMain {

	public static void main(String[] args) throws Exception{
				
		KieContainer kc = KieServices.Factory.get().getKieClasspathContainer();
		KieSession ksession = kc.newKieSession("FlujoCajaKS");
		
		ksession.addEventListener(new DebugAgendaEventListener());
		ksession.addEventListener(new DebugRuleRuntimeEventListener());
		
		Periodo periodo = new Periodo(date("2014-03-01"),date("2014-03-31"));
		
		Cuenta cuenta = new Cuenta(1,0);
		
		Movimiento m1 = new Movimiento(date("2014-04-12"),100,TipoMovimiento.DEBITO,1);
		Movimiento m2 = new Movimiento(date("2014-03-15"),20,TipoMovimiento.CREDITO,1);
		Movimiento m3 = new Movimiento(date("2014-03-21"),85,TipoMovimiento.DEBITO,1);
		Movimiento m4 = new Movimiento(date("2014-03-02"),45,TipoMovimiento.CREDITO,1);
		Movimiento m5 = new Movimiento(date("2014-02-24"),50,TipoMovimiento.CREDITO,1);
		
		FactHandle fh = ksession.insert(periodo);
		ksession.insert(cuenta);
		
		ksession.insert(m1);
		ksession.insert(m2);
		ksession.insert(m3);
		ksession.insert(m4);
		ksession.insert(m5);
		
		ksession.fireAllRules();
		
		 System.out.println("La cuenta "+cuenta.getNumeroCuenta() + " tiene: " 
				   + cuenta.getBalance() + " soles.");
		
		periodo.setInicio(date("2014-04-01"));
		periodo.setFin(date("2014-04-30"));
		
		ksession.update(fh, periodo);
		
		ksession.fireAllRules();
		
	}
	
	public static Date date(String cadena) throws Exception{
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		return sdf.parse(cadena);
	}
}
