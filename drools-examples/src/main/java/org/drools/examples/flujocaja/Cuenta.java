package org.drools.examples.flujocaja;

public class Cuenta {

	private int numeroCuenta;
	private int balance;
	
	public Cuenta(int numeroCuenta, int balance){
		this.numeroCuenta = numeroCuenta;
		this.balance = balance;
	}
	
	public int getNumeroCuenta() {
		return numeroCuenta;
	}
	public void setNumeroCuenta(int numeroCuenta) {
		this.numeroCuenta = numeroCuenta;
	}
	public int getBalance() {
		return balance;
	}
	public void setBalance(int balance) {
		this.balance = balance;
	}
}
