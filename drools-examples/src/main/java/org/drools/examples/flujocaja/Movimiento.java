package org.drools.examples.flujocaja;

import java.util.Date;

public class Movimiento {
	
	private Date fecha;
	private int monto;
	private int tipo;
	private int numeroCuenta;
	
	public Movimiento(Date fecha, int monto, int tipo, int numeroCuenta){
		this.fecha = fecha;
		this.monto = monto;
		this.tipo = tipo;
		this.numeroCuenta = numeroCuenta;
	}
	
	public Date getFecha() {
		return fecha;
	}
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	public int getMonto() {
		return monto;
	}
	public void setMonto(int monto) {
		this.monto = monto;
	}
	public int getTipo() {
		return tipo;
	}
	public void setTipo(int tipo) {
		this.tipo = tipo;
	}
	public int getNumeroCuenta() {
		return numeroCuenta;
	}
	public void setNumeroCuenta(int numeroCuenta) {
		this.numeroCuenta = numeroCuenta;
	}
}
