package org.drools.examples.helloworld;

import java.util.ArrayList;
import org.kie.api.KieServices;
import org.kie.api.event.rule.DebugAgendaEventListener;
import org.kie.api.event.rule.DebugRuleRuntimeEventListener;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;

public class HelloWorldExample {

	public static final void main(final String[] args){
		
		KieServices ks = KieServices.Factory.get();
		
		KieContainer kc = ks.getKieClasspathContainer();
		
		KieSession ksession = kc.newKieSession("HelloWorldKS");
	
		ksession.setGlobal("list", new ArrayList<Object>());
		
		ksession.addEventListener(new DebugAgendaEventListener());
		ksession.addEventListener(new DebugRuleRuntimeEventListener());
		
		final Mensaje mensaje = new Mensaje();
		mensaje.setTexto("Hola mundo!");
		mensaje.setEstado(Mensaje.HOLA);
		ksession.insert(mensaje);
		
//	    final Mensaje mensaje2 = new Mensaje();
//	    mensaje.setTexto("Chau mundo!");
//	    mensaje.setEstado(Mensaje.CHAU);
//		ksession.insert(mensaje2);
		
		ksession.fireAllRules();
		
		ksession.dispose();
	}
	
	
	public static class Mensaje{
		public static final int HOLA = 0;
		public static final int CHAU = 1;
		
		private String texto;
		private int estado;
		
		public Mensaje(){			
		}

		public String getTexto() {
			return texto;
		}

		public void setTexto(final String texto) {
			this.texto = texto;
		}

		public int getEstado() {
			return estado;
		}

		public void setEstado(final int estado) {
			this.estado = estado;
		}			
		
		
	}
}
