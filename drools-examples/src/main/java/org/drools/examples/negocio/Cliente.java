package org.drools.examples.negocio;

public class Cliente {
	
	 private String nombre;
	 private boolean registrado;
	 private float gasto;
	 private float descuento;
	 
	 public Cliente(String nombre, boolean registrado, float gasto)
	 {
		 this.nombre= nombre;
		 this.registrado = registrado;
		 this.gasto = gasto;
	 }
	 
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public float getGasto() {
		return gasto;
	}
	public void setGasto(float gasto) {
		this.gasto = gasto;
	}
	public float getDescuento() {
		return descuento;
	}
	public void setDescuento(float descuento) {
		this.descuento = descuento;
	}
	public boolean isRegistrado() {
		return registrado;
	}
	public void setRegistrado(boolean registrado) {
		this.registrado = registrado;
	}
}
