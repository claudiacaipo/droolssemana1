package org.drools.examples.negocio;

import java.util.ArrayList;

import org.drools.examples.negocio.Cliente;
import org.kie.api.KieServices;
import org.kie.api.event.rule.DebugAgendaEventListener;
import org.kie.api.event.rule.DebugRuleRuntimeEventListener;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;

public class DescuentosMain {

	public static void main(String[] args) {
		
		KieServices ks = KieServices.Factory.get();
		
		KieContainer kc = ks.getKieClasspathContainer();
		
		KieSession ksession = kc.newKieSession("DescuentoKS");
			
		ksession.addEventListener(new DebugAgendaEventListener());
		ksession.addEventListener(new DebugRuleRuntimeEventListener());
		
		// Cliente no registrado que gasta m�s de 1000 soles
		  Cliente cliente1 = new Cliente("Cliente 1", false, 1200);
		  ksession.insert(cliente1);
		   
		  // Cliente registrado que gasta menos de 1000 soles
		  Cliente cliente2 = new Cliente("Cliente 2", true, 800);
		  ksession.insert(cliente2);
		   
		  // Cliente registrado que gasta m�s de 1000 soles
		  Cliente cliente3 = new Cliente("Cliente 3", true, 1600);
		  ksession.insert(cliente3);
		  
		  // Cliente no registrado que gasta menos de 1000 soles
		  Cliente cliente4 = new Cliente("Cliente 4", false, 700);
		  ksession.insert(cliente4);
		   
		  ksession.fireAllRules();
		  		   
		  System.out.println("El cliente 1 tiene un descuento de " 
		   + cliente1.getDescuento() + " soles.");
		  System.out.println("El cliente 2 tiene un descuento de " 
		   + cliente2.getDescuento() + " soles.");
		  System.out.println("El cliente 3 tiene un descuento de " 
		   + cliente3.getDescuento() + " soles.");
		  System.out.println("El cliente 4 tiene un descuento de " 
				   + cliente4.getDescuento() + " soles.");
		  
		  ksession.dispose();
	}

}
